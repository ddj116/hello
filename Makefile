.PHONY: all clean build-centos8  run-centos8
DOCKER_RELEASE_TAG=v1.0
ME=$(shell whoami)

GCC=g++
INCLUDE=-I./include

all: hello
hello: src/hello.cpp src/box.o
	$(GCC) -Wall -O0 -g -o hello $(INCLUDE) src/*.o src/hello.cpp
clean:
	rm -f hello src/*.o

src/box.o : src/box.cpp
	$(GCC) -c -o src/box.o src/box.cpp $(CFLAGS) $(LDFLAGS) $(INCLUDE)



build-centos8-release:
	docker build -t registry.gitlab.com/ddj116/hello/hello-centos8:$(DOCKER_RELEASE_TAG) -f docker/centos8/Dockerfile .
build-centos8:
	docker build --build-arg myuser=$(shell whoami) -t hello-centos8-$(ME) -f docker/centos8/Dockerfile.user .

run-centos8: build-centos8
	docker run -ti --rm hello-centos8-$(ME)

run-centos8-local: build-centos8
	docker run -ti --rm \
        -e HOME=${HOME} \
        -v "${HOME}:${HOME}/" \
        -v /etc/group:/etc/group:ro \
        -v /etc/passwd:/etc/passwd:ro \
        --security-opt seccomp=unconfined \
        -u $(shell id -u ${USER} ):$(shell id -g ${USER} ) \
        hello-centos8-$(ME)

help:
	# Available targets:
	# all (default):         Build the hello binary
	# clean:                 Clean untracked binaries
	# build-centos8:         Build centos8 docker image
	# build-centos8-release: Build centos8 "release" (base) image
	# run-centos8:           Run centos8 base image (as root)
	# run-centos8-local:     Run centos8 user image (as you)
